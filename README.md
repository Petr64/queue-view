# Laravel 5's Queue Manager



## Install

Via Composer

``` bash
$ composer require grambas/queue-view dev-master
```

## Usage

1- Install

Only for Laravel <= 5.2, add ServiceProvider to config/app.php

```config/app.php
'providers' => [
  Grambas\QueueView\QueueViewServiceProvider::class,
]

```

2- Use :

http://site.com/QueueView

or 

```
@include('QueueView::table') {{-- in any blade --}}
```

or

```
php artisan vendor:publish 
```
publish QueueView config and edit it in config/ folder


