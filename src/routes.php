<?php 

Route::get(config('QueueView.route') . '/', [		'as' => 'QueueView.index',		'uses' => 'Petr64\QueueView\QueueViewController@index']);
Route::get(config('QueueView.route') . '/get', [		'as' => 'QueueView.get',	'uses' => 'Petr64\QueueView\QueueViewController@queueGet']);
Route::get(config('QueueView.route') . '/delete/{id}',[ 'as'=>'QueueView.delete', 'uses' => 'Petr64\QueueView\QueueViewController@queueDelete']);


Route::get(config('QueueView.route') . '/failed', [	'as' => config('QueueView.route').'.failed.index',	'uses' => 'Petr64\QueueView\QueueViewController@failedIndex']);
Route::get(config('QueueView.route') . '/failed/get', [	'as' => config('QueueView.route').'.failed.get',	'uses' => 'Petr64\QueueView\QueueViewController@failedGet']);
Route::get(config('QueueView.route') . '/failed/delete/{id}', ['as' => config('QueueView.route').'.failed.delete',	'uses' => 'Petr64\QueueView\QueueViewController@FailedDelete']);
