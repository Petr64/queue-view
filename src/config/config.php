<?php return [
    
    /*
    |--------------------------------------------------------------------------
    | Package Configuration Option
    |--------------------------------------------------------------------------
    | Describe what it does. 
    */

    //Pacakge view slug By default 'queue-view'
    'route'	 => 'QueueView',
    //Rows per page
    'perPage'	 => '20',


    //Queue Table. By default 'Jobs'
    'tableQueue' => 'jobs',

    
    //Enable or Disable failed Jobs
    'failedJobs'	 => false,
    'tableFailed' => 'failed_jobs',

];
