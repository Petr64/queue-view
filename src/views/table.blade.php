<div class="container">

    <div class="row">

        <div class="col-lg-12 margin-tb">
            
            <table class="table table-bordered" id="table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Queue</th>
                        <th>Payload</th>
                        <th>Attempts</th>
                        <th>Reserved At</th>
                        <th>Available At</th>
                        <th>Created At</th>
                        <th>Action</th>
                        </tr>
                </thead>
            </table>

        </div>
        
    </div>
    
</div>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(function() {
    $('#table').DataTable({
        processing: true,
        serverSide: true,
        "pageLength":{{config('QueueView.perPage')}},
        ajax: '{!! route('QueueView.get') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'queue', name: 'queue' },
            { data: 'payload', name: 'payload' },
            { data: 'attempts', name: 'attempts' },
            { data: 'reserved_at', name: 'reserved_at' },
            { data: 'available_at', name: 'available_at' },
            { data: 'created_at', name: 'created_at' },
            {data: 'action', name: 'action', orderable: false, searchable: false},
           
        ]
    });
});
</script>